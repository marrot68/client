/*
 * Copyright (C) 2010-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.sheepit.client.os;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.software.os.OperatingSystem;
import oshi.hardware.HardwareAbstractionLayer;
import com.sheepit.client.hardware.cpu.CPU;

public abstract class OS {
	private static SystemInfo systemInfo = new SystemInfo();
	
	public static OperatingSystem operatingSystem = systemInfo.getOperatingSystem();
	
	private static HardwareAbstractionLayer hardwareAbstractionLayer = systemInfo.getHardware();
	
	private static OS instance = null;
	
	/**
	 * Operating system name
	 * @return a string representing the OS name.
	 */
	public abstract String name();
	
	/**
	 * Blender requires 64 bits
	 * @return boolean if we are supported i.e. if we run on 64 bits
	 */
	public boolean isSupported() {  return "64bit".equals(getCPU().getArch()); }
	
	/** Get the full version of the operating system.
	 * For example windows, should give "windows 8.1"
	 * @return a lowercassed string with the os name and the version info
	 * @see OperatingSystem#getVersionInfo()
	 */
	public String getVersion() {
		return (name() + " " + operatingSystem.getVersionInfo()).toLowerCase();
	}
	
	/**
	 * @see oshi.hardware.GlobalMemory#getTotal()
	 * @return long of getTotal() in kilobytes
	 */
	public long getTotalMemory() {
		return hardwareAbstractionLayer.getMemory().getTotal() / 1024;
	}
	
	/**
	 * @see oshi.hardware.GlobalMemory#getAvailable()
	 * @return long of getAvailable() in kilobytes
	 */
	public long getFreeMemory() {
		return hardwareAbstractionLayer.getMemory().getAvailable() / 1024;
	}
	
	/**
	 * Get the path to the Blender executable
	 * @return a string representing a path to Blender,
	 * relative or absolute depends on the implementation
	 */
	public abstract String getRenderBinaryPath();
	
	/**
	 * Get the path to the CUDA library
	 * @return a string representing a path the CUDA library,
	 * relative or absolute depends on the implementation
	 */
	public String getCUDALib() {
		return null;
	}
	
	/**
	 * Get the path to the NVIDIA NVML library
	 * @return a string representing a path the NVIDIA NVML library,
	 * relative or absolute depends on the implementation
	 */
	public String getNVMLLib() {
		return null;
	}
	
	/**
	 * Determines if the platform supports setting a higher priority
	 * @return a boolean representing if the platform supports setting a higher priority
	 */
	public abstract boolean isHighPrioritySupported();
	
	/**
	 * Determines if the platform supports being IO nice
	 * @return a boolean representing if the platform supports being IO nice
	 */
	public abstract boolean isNiceAvailable();
	
	/**
	 * Shuts the computer down waiting allow all SheepIt threads to close and exit
	 * @param delayInMinutes integer repressing the amount minutes to wait before shutting down
	 */
	public abstract void shutdownComputer(int delayInMinutes);
	
	/**
	 * Creates and populates a CPU object
	 * @return a populated CPU object
	 */
	public CPU getCPU() {
		CentralProcessor.ProcessorIdentifier cpuID = hardwareAbstractionLayer.getProcessor().getProcessorIdentifier();
		CPU ret = new CPU();
		ret.setName(cpuID.getName());
		ret.setModel(cpuID.getModel());
		ret.setFamily(cpuID.getFamily());
		return ret;
	}
	
	/**
	 * Executes a Process
	 * @param command List of strings of the command and the respective command line arguments
	 * @param env Map of strings of environment variables and their values for the execution
	 * @return a Process object reflecting the execution of the command
	 * @throws IOException if an I/O error occurs
	 */
	public abstract Process exec(List<String> command, Map<String, String> env) throws IOException;
	
	/**
	 * Terminates a process
	 * @param proc Process to kill
	 * @return true if proc wasn't null and was destroyed
	 */
	public boolean kill(Process proc) {
		if (proc != null) {
			proc.destroy();
			return true;
		}
		return false;
	}
	
	/**
	 * Get the operating system currently in use
	 * @return object instance of OS specific to the used operating system.
	 */
	public static OS getOS() {
		if (instance == null) {
			switch (operatingSystem.getManufacturer()){
				case "Microsoft":
					instance = new Windows();
					break;
				case "Apple":
					if ("aarch64".equalsIgnoreCase(System.getProperty("os.arch"))) { // Set as M1 Mac if we are on ARM64
						instance = new MacM1();
					}
					else {
						instance = new Mac();
					}
					break;
				case "GNU/Linux":
					instance = new Linux();
					break;
			}
		}
		return instance;
	}
	
	/**
	 * Returns the default path that SheepIt would place the config file
	 * This respects the XDG specification for Linux and macOS
	 * @see <a href="https://www.freedesktop.org/wiki/Software/xdg-user-dirs/">XDG specification</a>
	 * @return string representing the default path to the sheepit.conf
	 */
	public String getDefaultConfigFilePath() {
		String xdgConfigHome = System.getenv("XDG_CONFIG_HOME");
		String userHome = System.getProperty("user.home");
		// fallback xdg config home should be ~/.config/
		if (xdgConfigHome == null || xdgConfigHome.isEmpty()) {
			xdgConfigHome = userHome + File.separator + ".config";
		}
		// add the config folder to the path 
		xdgConfigHome += File.separator + "sheepit";
		
		// check if file already exists in ~/.config/sheepit/sheepit.conf
		File file = new File(xdgConfigHome + File.separator + "sheepit.conf");
		if (file.exists()) {
			return file.getAbsolutePath();
		} 
		// if it doesn't exist, try $HOME/.sheepit.conf
		file = new File(userHome + File.separator + ".sheepit.conf");
		if (file.exists()) {
			return file.getAbsolutePath();
		}
		// if it doesn't exist, create the file in the XDG compliant location
		file = new File(xdgConfigHome);
		file.mkdirs();
		return file.getAbsolutePath() + File.separator + "sheepit.conf";
	}
}
