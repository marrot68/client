package com.sheepit.client.hardware.gpu.hip;

import com.sheepit.client.hardware.gpu.hip.data.HipError_t;
import com.sheepit.client.os.OS;
import com.sheepit.client.os.Windows;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;

public class HIP {
	
	private static final String HIP_LIBRARY = "amdhip64";
	public static final String TYPE = "HIP";
	
	private static int HIP_DEVICES_CACHED = -1;	//Store number of retrieved devices to save sys calls
	
	
	private static int getNumberOfDevices(HIPLib hipLib) {
		IntByReference deviceCount = new IntByReference();
		int status = hipLib.hipGetDeviceCount(deviceCount);
		if (status != HipError_t.HIP_SUCCESS) {
			System.err.println("Error");
			return -1;
		}
		return deviceCount.getValue();
	}
	

	
	public static boolean hasHIPDevices() {
		
		if (HIP_DEVICES_CACHED >= 0) {
			return HIP_DEVICES_CACHED > 0;
		}
		
		OS os = OS.getOS();
		if (os instanceof Windows) {
		
			try {
				HIPLib hip = (HIPLib) Native.load(HIP_LIBRARY, HIPLib.class);
				HIP_DEVICES_CACHED = getNumberOfDevices(hip);
			}
			catch (java.lang.UnsatisfiedLinkError e) {
				System.out.println("HIP::getGpus failed(A) to load HIP lib (path: " + HIP_LIBRARY + ")");
			}
			catch (java.lang.ExceptionInInitializerError e) {
				System.out.println("HIP::getGpus failed(B) ExceptionInInitializerError " + e);
			}
			catch (Exception e) {
				System.out.println("HIP::getGpus failed(C) generic exception " + e);
			}
			if (HIP_DEVICES_CACHED < 0) {
				HIP_DEVICES_CACHED = 0;
			}
		}
		else {
			HIP_DEVICES_CACHED = 0;
		}
		
		return HIP_DEVICES_CACHED > 0;
	}
}
